import java.io.File;
import java.io.FileNotFoundException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public abstract class TestHelper {

    static public String generateDate(){
        GregorianCalendar calendar = new GregorianCalendar();
        int year = randBetween(1970, 2021);
        calendar.set(Calendar.YEAR, year);
        int dayOfYear = randBetween(1, calendar.getActualMaximum(calendar.DAY_OF_YEAR));
        calendar.set(calendar.DAY_OF_YEAR, dayOfYear);
        var getYear = String.valueOf(calendar.get(calendar.YEAR)) ;
        var getMounth = String.valueOf(calendar.get(calendar.MONTH)+1) ;
        var getDay = String.valueOf(calendar.get(calendar.DAY_OF_MONTH)) ;
        return getYear+"-"+getMounth+"-"+getDay;
    }

    private static int randBetween(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }

    static public long generateLongNumber(){
        long longNumber = randBetweenLong(12345678910L, 12345678909L);
        return longNumber;
    }
    private static long randBetweenLong(long start, long end) {
        return start + (long) Math.round(Math.random() * (end - start));
    }

    static public String generateString(){
        Random rnd = new Random(10);
        String str = "";
        for (int i = 0; i < 3; i++) {
            str += (getGenerateRandomString(rnd.nextInt(6) + 5)+ " ");
        }
        return str;
    }

    private static String getGenerateRandomString(int length) {
        final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
        final String DATA_FOR_RANDOM_STRING = CHAR_LOWER;
        SecureRandom random = new SecureRandom();
        if (length < 1) throw new IllegalArgumentException();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
            char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);
            sb.append(rndChar);
        }
        String str = sb.toString();
        String strWithUpperCh = str.substring(0, 1).toUpperCase() + str.substring(1);
        return strWithUpperCh;
    }

    static public HashMap readFile(){
        ArrayList<String> data = new ArrayList();
        HashMap<String, String> values = new HashMap<>();
        try {
            File myObj = new File("src/main/resources/filename.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                data.add(myReader.nextLine());

                for (String item: data) {
                    String[] arr = item.split("::");
                    for(String item1: arr){
                        values.put(arr[0], arr[1]);
                    }
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        printHashMap(values);

        return values;
    }

    private static void printHashMap(HashMap<String, String> values) {
        values.forEach((key, value) -> {
            System.out.println(key + "=" + value + " ");
        });
    }

    static public String formatDate(String str)  {
        DateFormat originalFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = null;
        try {
            date = originalFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = targetFormat.format(date);
        return formattedDate;
    }

    static public double stringToDoubleOrinfinity(String txt){

        try {
            Double value = Double.parseDouble(txt);
            return value;
        }catch (Exception exp){
            return Double.POSITIVE_INFINITY;
        }
    }
}
