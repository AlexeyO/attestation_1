import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class TestHelperTest {

    @Test
    void generateDateTest() {
        String strDate = MyClass.generateDate();
        boolean result = strDate.matches("\\d{4}-\\d{1,2}-\\d{1,2}");
        assertEquals(result, true);
    }

    @Test
    void generateLongNumberTest() {
        long longNumber = MyClass.generateLongNumber();
        Long val = Long.valueOf(longNumber);
        assertEquals(longNumber, val);
    }

    @Test
    void generateStringTest() {
        String strDate = MyClass.generateString();
        assertEquals(false, strDate.isEmpty());
    }

    @Test
    void readFileTest() {
        HashMap values = MyClass.readFile();
        assertEquals("value1", values.get("Key1"));
    }

    @Test
    void formatDateTest() {
         String strDate = "October 15, 2021";
         String result = MyClass.formatDate(strDate);
         assertEquals(result, "20211015");
    }

    @Test
    void stringToDoubleOrinfinityTest() {
         String text1 = "8";
         String text2 = "qeq";
         double value1 =  MyClass.stringToDoubleOrinfinity(text1);
         double value2 =  MyClass.stringToDoubleOrinfinity(text2);
         assertEquals(8.0, value1 );
         assertEquals(Double.POSITIVE_INFINITY, value2);

    }


}